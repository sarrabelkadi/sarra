<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use DB;
use App\Quotation;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userr= User::all();  
return view('index',compact('userr'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // form view to create a User
        return view('create');

}
public function search(Request $request)
{
    $search = $request->get('search');
    $userr = DB::table('users')->where('name','like','%'.$search.'%')->paginate(5);
    return view('index',['userr' => $userr]);

}
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->authorize('create',User::class);
        $this->validate($request,[
            'name' => 'required|min:3',
            'email' => 'required|email',
            'password' => 'required|min:8|max:50',
            'age' => 'required'
        ]);
        $data = $request->all();
        $data["password"] = bcrypt($data["password"]);
        User::create($data);
        return redirect()->route('userr.store')->with('alert','User has been created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return $user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // return view('dashboard.users.form',["user" => $user]);
         $userr = User::find($id);
        return view('edit',compact('userr'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $userr)
    {
        // $this->validate()
        $this->authorize('update',$userr);
       $userr->update($request->all());
        return redirect()->route('userr.index')->with('message','User has been updated');
        //$this->authorize('edit',User::class);
         //$this->validate($request,[
          //'name' => 'required',
          //'email' => 'required',
        //]);
        //User::find($id)->update($request->all());
        //return redirect()->route('userr.index')->with('success','Post update success');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(user $userr)
    {
        $this->authorize('delete',$userr);
        $userr->delete();
        return redirect()->route('userr.index')->with('message','user has been deleted');
        
    }
}
