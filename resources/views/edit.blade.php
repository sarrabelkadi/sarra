@extends('layouts.app')
@section('content')
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      {{ Form::model($userr,['route'=>['userr.update',$userr->id],'method'=>'PATCH']) }}
      @include('fourma')
      {{ Form::close() }}
    </div>
  </div>
@endsection