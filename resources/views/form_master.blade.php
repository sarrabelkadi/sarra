<div class="row">
  <div class="col-sm-2">
    {!! form::label('name','name') !!}
  </div>
  <div class="col-sm-10">
    <div class="form-group {{ $errors->has('name') ? 'has-error' : "" }}">
      {{ Form::text('name',NULL, ['class'=>'form-control', 'id'=>'title', 'placeholder'=>'name...']) }}
      {{ $errors->first('title', '<p class="help-block">:message</p>') }}
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-2">
    {!! form::label('email','email') !!}
  </div>
  <div class="col-sm-10">
    <div class="form-group {{ $errors->has('email') ? 'has-error' : "" }}">
      {{ Form::text('email',NULL, ['class'=>'form-control', 'id'=>'body', 'placeholder'=>'email...']) }}
      {{ $errors->first('body', '<p class="help-block">:message</p>') }}
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-2">
    {!! form::label('password','password') !!}
  </div>
  <div class="col-sm-10">
    <div class="form-group {{ $errors->has('password') ? 'has-error' : "" }}">
      {{ Form::text('password',NULL, ['class'=>'form-control', 'id'=>'body', 'placeholder'=>'password...']) }}
      {{ $errors->first('body', '<p class="help-block">:message</p>') }}
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-2">
    {!! form::label('age','age') !!}
  </div>
  <div class="col-sm-10">
    <div class="form-group {{ $errors->has('password') ? 'has-error' : "" }}">
      {{ Form::text('age',NULL, ['class'=>'form-control', 'id'=>'body', 'placeholder'=>'password...']) }}
      {{ $errors->first('body', '<p class="help-block">:message</p>') }}
    </div>
  </div>
</div>
<div class="form-group">
  {{ Form::button(isset($model)? 'Update' : 'Ajouter' , ['class'=>'btn btn-success', 'type'=>'submit']) }}
</div>