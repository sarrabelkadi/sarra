

@extends('layouts.app')
@section('content')
  <div class="row">
  <div class="col-md-4" >
    <div class="full-right">
      <div class="col-md-12" style="height:50px; width:1000px">
      
  <form action="/search" method="get">
    <div class="input-group">
      <input type="search" name="search" class="form-control">
      <span class="input-group-prepend">
        <button type="submit" class="btn btn-primary"> Search </button>
      </span>
    </div>
  </form>
</div>
    </div>
  </div>
  </div>

  @if ($message = Session::get('success'))
      <div class="alert alert-success">
          <p>{{ $message }}</p>
      </div>
  @endif

  <table class="table table-bordered" border="3">
    <tr>
     <th with="80px">Numéro de l'utilisateur</th>
      <th>name</th>
      <th>E-mail</th>
      @can('create',App\User::class)
       <th with="140px" class="text-center">
        <a href="{{route('userr.create')}}" class="btn btn-success btn-sm">
          <i class="glyphicon glyphicon-plus"> </i>
        </a>
      </th>
      @endcan
    </tr>
   <?php $no=1; ?>
    @foreach ($userr as $key => $value)
 
      <tr>
     <td>{{$no++}}</td>
        <td>{{ $value->name }}</td>
        <td>{{ $value->email}}</td>
        
      
        <td>
         <a class="btn btn-primary btn-sm" href="{{route('userr.edit',$value->id)}}">
              <i class="glyphicon glyphicon-pencil">  </i></a>
              {!! Form::open(['method' => 'DELETE','route' => ['userr.destroy', $value->id],'style'=>'display:inline']) !!}
              <button type="submit" style="display: inline;" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-trash"> </i></button>
            {!! Form::close() !!} </td> 
     </tr>
     
    @endforeach
  </table>
@endsection