<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/user/home', 'UserController@index')->name('home');

Route::get('/home', 'HomeController@index');
//Route::resource('admin/users','UserController')->middleware('auth');
Route::get('/search','UserController@search');
Route::resource('userr','UserController')->middleware('Age');